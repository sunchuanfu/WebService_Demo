/**
 * WeatherWebServiceTestCase.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package weather;

public class WeatherWebServiceTestCase extends junit.framework.TestCase {
    public WeatherWebServiceTestCase(java.lang.String name) {
        super(name);
    }

    public void testWeatherWebServiceSoapWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new weather.WeatherWebServiceLocator().getWeatherWebServiceSoapAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new weather.WeatherWebServiceLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1WeatherWebServiceSoapGetSupportCity() throws Exception {
        weather.WeatherWebServiceSoap_BindingStub binding;
        try {
            binding = (weather.WeatherWebServiceSoap_BindingStub)
                    new weather.WeatherWebServiceLocator().getWeatherWebServiceSoap();
        } catch (javax.xml.rpc.ServiceException jre) {
            if (jre.getLinkedCause() != null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getSupportCity(new java.lang.String());
        // TBD - validate results
    }

    public void test2WeatherWebServiceSoapGetSupportProvince() throws Exception {
        weather.WeatherWebServiceSoap_BindingStub binding;
        try {
            binding = (weather.WeatherWebServiceSoap_BindingStub)
                    new weather.WeatherWebServiceLocator().getWeatherWebServiceSoap();
        } catch (javax.xml.rpc.ServiceException jre) {
            if (jre.getLinkedCause() != null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getSupportProvince();
        // TBD - validate results
    }

    public void test3WeatherWebServiceSoapGetSupportDataSet() throws Exception {
        weather.WeatherWebServiceSoap_BindingStub binding;
        try {
            binding = (weather.WeatherWebServiceSoap_BindingStub)
                    new weather.WeatherWebServiceLocator().getWeatherWebServiceSoap();
        } catch (javax.xml.rpc.ServiceException jre) {
            if (jre.getLinkedCause() != null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        weather.GetSupportDataSetResponseGetSupportDataSetResult value = null;
        value = binding.getSupportDataSet();
        // TBD - validate results
    }

    public void test4WeatherWebServiceSoapGetWeatherbyCityName() throws Exception {
        weather.WeatherWebServiceSoap_BindingStub binding;
        try {
            binding = (weather.WeatherWebServiceSoap_BindingStub)
                    new weather.WeatherWebServiceLocator().getWeatherWebServiceSoap();
        } catch (javax.xml.rpc.ServiceException jre) {
            if (jre.getLinkedCause() != null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);
        // Time out after a minute
        binding.setTimeout(60000);
        // Test operation
        java.lang.String[] value = null;
        value = binding.getWeatherbyCityName("北京");
        for (String str : value) {
            System.out.println(str);
        }
        // TBD - validate results
    }

    public void test5WeatherWebServiceSoapGetWeatherbyCityNamePro() throws Exception {
        weather.WeatherWebServiceSoap_BindingStub binding;
        try {
            binding = (weather.WeatherWebServiceSoap_BindingStub)
                    new weather.WeatherWebServiceLocator().getWeatherWebServiceSoap();
        } catch (javax.xml.rpc.ServiceException jre) {
            if (jre.getLinkedCause() != null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getWeatherbyCityNamePro(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void testWeatherWebServiceSoap12WSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new weather.WeatherWebServiceLocator().getWeatherWebServiceSoap12Address() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new weather.WeatherWebServiceLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test6WeatherWebServiceSoap12GetSupportCity() throws Exception {
        weather.WeatherWebServiceSoap12Stub binding;
        try {
            binding = (weather.WeatherWebServiceSoap12Stub)
                    new weather.WeatherWebServiceLocator().getWeatherWebServiceSoap12();
        } catch (javax.xml.rpc.ServiceException jre) {
            if (jre.getLinkedCause() != null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getSupportCity(new java.lang.String());
        // TBD - validate results
    }

    public void test7WeatherWebServiceSoap12GetSupportProvince() throws Exception {
        weather.WeatherWebServiceSoap12Stub binding;
        try {
            binding = (weather.WeatherWebServiceSoap12Stub)
                    new weather.WeatherWebServiceLocator().getWeatherWebServiceSoap12();
        } catch (javax.xml.rpc.ServiceException jre) {
            if (jre.getLinkedCause() != null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getSupportProvince();
        // TBD - validate results
    }

    public void test8WeatherWebServiceSoap12GetSupportDataSet() throws Exception {
        weather.WeatherWebServiceSoap12Stub binding;
        try {
            binding = (weather.WeatherWebServiceSoap12Stub)
                    new weather.WeatherWebServiceLocator().getWeatherWebServiceSoap12();
        } catch (javax.xml.rpc.ServiceException jre) {
            if (jre.getLinkedCause() != null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        weather.GetSupportDataSetResponseGetSupportDataSetResult value = null;
        value = binding.getSupportDataSet();
        // TBD - validate results
    }

    public void test9WeatherWebServiceSoap12GetWeatherbyCityName() throws Exception {
        weather.WeatherWebServiceSoap12Stub binding;
        try {
            binding = (weather.WeatherWebServiceSoap12Stub)
                    new weather.WeatherWebServiceLocator().getWeatherWebServiceSoap12();
        } catch (javax.xml.rpc.ServiceException jre) {
            if (jre.getLinkedCause() != null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getWeatherbyCityName(new java.lang.String());
        // TBD - validate results
    }

    public void test10WeatherWebServiceSoap12GetWeatherbyCityNamePro() throws Exception {
        weather.WeatherWebServiceSoap12Stub binding;
        try {
            binding = (weather.WeatherWebServiceSoap12Stub)
                    new weather.WeatherWebServiceLocator().getWeatherWebServiceSoap12();
        } catch (javax.xml.rpc.ServiceException jre) {
            if (jre.getLinkedCause() != null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getWeatherbyCityNamePro(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

}
